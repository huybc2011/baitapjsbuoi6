// bài tập 1
//Tìm số nguyên dương nhỏ nhất sao cho:
//1 + 2 + … + n > 10000
document.getElementById("txt-xac-nhan").addEventListener("click", function () {
  var sum = 0;
  var number = 0;
  while (sum < 10000) {
    number++;
    sum += number;
  }
  console.log(number);
  document.getElementById(
    "ket-qua"
  ).innerHTML = ` <div>Số nguyên dương nhỏ nhất là : ${number}</div>`;
});
// bài tập 2
// Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
// + x^3 + … + x^n (Sử dụng vòng lặp và hàm)
document.getElementById("txt-ket-qua").addEventListener("click", function () {
  var soX = document.getElementById("txt-so-x").value * 1;
  var soN = document.getElementById("txt-so-n").value * 1;
  var lt = 1;
  var ketQua = 0;
  for (var i = 1; i <= soN; i++) {
    lt = soX ** i;
    ketQua += lt;
  }
  console.log(ketQua);
  document.getElementById(
    "txt-tong"
  ).innerHTML = `<div>Tổng là : ${ketQua}</div>`;
});
// bài tập 3:
//Nhập vào n. Tính giai thừa 1*2*...n
document.getElementById("txt-giai-thua").addEventListener("click", function () {
  var soNguyenN = document.getElementById("so-n").value * 1;
  var gt = 1;

  for (a = 1; a <= soNguyenN; a++) {
    gt = gt * a;
    console.log("giai thừa", gt);
  }
  console.log("giai thừa", gt);
  document.getElementById(
    "giai-thua"
  ).innerHTML = `<div>Giai thừa : ${gt}</div>`;
});
// bài tập 4:
// Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
// Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
// background màu xanh.

document.getElementById("chan-le").addEventListener("click", function () {
  var layChanLe = function (i) {
    if (i % 2 == 0) {
      return `<div class="bg-danger"> Div chẵn ${i}</div>`;
    } else {
      return `<div class="bg-primary"> Div lẻ ${i}</div>`;
    }
  };
  var content = "";
  for (var i = 1; i <= 10; i++) {
    content += layChanLe(i);
  }
  document.getElementById("tao-div").innerHTML = `<div><br /> ${content}</div>`;
});
